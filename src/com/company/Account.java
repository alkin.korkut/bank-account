package com.company;

public class Account {
    private int number;
    private double balance;
    private String currency;

    public Account(int number, double balance, String currency){
        this.number = number;
        if(balance>0){
            this.balance = balance;
        }else{
            this.balance = 0;
        }

        this.checkSetCurrency(currency);

    }

    public Account(int number, String currency){
        this(number, 0, currency);
    }

    public Account(int number){
        this(number, 0, "TL");
    }

    public int getNumber(){
        return this.number;
    }

    public double getBalance(){
        return this.balance;
    }

    public String getCurrency(){
        return this.currency;
    }

    public void setCurrency(String currency){
        if(this.currency.equals("TL") && currency.equals("USD")){
            balance = balance / 2.9;
        }
        if(this.currency.equals("USD") && currency.equals("TL")){
            balance = balance * 2.9;
        }
        if(currency.equals("TL") || currency.equals("USD")){
            this.currency = currency;
        }
    }

    private void checkSetCurrency(String c){
        if(c.equals("USD")){
            currency = c;
        }else{
            currency = "TL";
        }
    }

    public void deposit(double d){
        if(d>0){
            balance = balance + d;
            System.out.println(d + " "+ currency + " have been deposited.");
            System.out.println("The balance is " + balance + " "+ currency);
        }else{
            System.out.println("The amount should be positive!");
        }
    }

    public void withdraw(double w){
        if(w>0){
            if(balance<w){
                System.out.println("Account does not have " + w + " "+ currency);
            }else{
                balance = balance - w;
                System.out.println(w + " " + currency + " have been withdrawn.");
                System.out.println("The balance is "+ balance + " " + currency);
            }
        }else{
            System.out.println("The amount should be positive!");
        }
    }

    public void report(){
        System.out.println("Account "+ number + " has "+ balance + " " + currency + ".");
    }

    public String toString(){
        return "Account "+ number + ": "+ balance + " "+ currency;
    }


}