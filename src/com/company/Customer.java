package com.company;

public class Customer {
    private String name;
    private Account account;

    public Customer(String name, Account account){
        this.name = name;
        this.account = account;
    }

    public String getName(){
        return this.name;
    }

    public Account getAccount(){
        return this.account;
    }

    public void deposit(double amount){
        this.account.deposit(amount);
    }

    public void withdraw(double amount){
        this.account.withdraw(amount);
    }

    public void report(){
        System.out.println("Customer "+ this.name + " ");
        this.account.report();
    }
}
